package com.example.userservice.unit.user;

import com.example.userservice.configuration.exceptionHandler.UserNotFoundException;
import com.example.userservice.controller.users.requests.CreateUserRequest;
import com.example.userservice.controller.users.requests.UpdateUserDetailRequest;
import com.example.userservice.controller.users.response.CreateUserResponse;
import com.example.userservice.fixtures.UserFixture;
import com.example.userservice.model.users.User;
import com.example.userservice.persistence.users.IUserRepo;
import com.example.userservice.service.users.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private IUserRepo userRepo;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userService;



    @Test
    void CreateMemberTest() {
        //Arrange
        CreateUserRequest request = UserFixture.getCreateMemberRequest();
        User user = UserFixture.getUser();
        when(userRepo.findByUsername(request.getUsername()))
                .thenReturn(Optional.empty());
        when(passwordEncoder.encode(Mockito.any(CharSequence.class)))
                .thenReturn("encodedPassword");
        when(userRepo.save(Mockito.any(User.class)))
                .thenReturn(user);
        //Act
        CreateUserResponse response = userService.createUser(request);
        //Assert
        assertEquals(request.getFirstName(), response.getFirstName());

    }

    @Test
    void SaveMemberWithExistingUsernameTest() {
        //Arrange
        CreateUserRequest request = UserFixture.getCreateMemberRequest();
        User exsistingUser = UserFixture.getUser();
        Mockito.when(userRepo.findByUsername(request.getUsername()))
                .thenReturn(Optional.of(exsistingUser));
        //Act and Assert
        assertThrows(UserNotFoundException.class, () -> userService.saveUser(request));
    }

    @Test
    void UpdateMemberDetailsTest() {
        //Assert
        UUID userId = UUID.randomUUID();
        User user = UserFixture.getUser();
        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(userRepo.save(user)).thenReturn(user);
        //Act
        UpdateUserDetailRequest request = UserFixture.getUpdateMemberRequest();
        User updatedUser = userService.updateUser(request, userId);
        //Assert
        assertEquals("UsernameTest", updatedUser.getUsername());
        assertEquals("test_edit@gmail.com", updatedUser.getEmail());
        assertEquals("EditedScreenName", updatedUser.getScreenName());
    }


//    @Test
//    void FindAllUsersTest() {
//        //Arrange
//        List<User> users = UserFixture.getUsers(2);
//        Mockito.when(userRepo.findAllByRole(User.Role.MEMBER)).thenReturn(users);
//        //Act
//        List<User> result = userService.getAllUsers();
//        //Assert
//        assertEquals(2, result.size());
//        assertEquals("TestFirst-0", result.get(0).getFirstname());
//        assertEquals("TestLast-0", result.get(0).getLastname());
//        assertEquals("TestFirst-1", result.get(1).getFirstname());
//        assertEquals("TestLast-1", result.get(1).getLastname());
//    }

}

## Define PostgreSQL connection details
#DB_HOST="PostgreSQL"
#DB_PORT="5432"
#DB_NAME="applicatus_user"
#DB_USER="postgres"
#DB_PASSWORD="admin"
#
#if psql -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" -lqt | cut -d \| -f 1 | grep -qw "$DB_NAME"; then
#    echo "Database '$DB_NAME' already exists."
#else
#    # Create the database
#    echo "Creating database '$DB_NAME'..."
#    createdb -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" "$DB_NAME"
#
#    # Add any additional initialization steps if needed
#    # psql -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" "$DB_NAME" < path/to/your/init_script.sql
#
#    echo "Database '$DB_NAME' created successfully."
#fi
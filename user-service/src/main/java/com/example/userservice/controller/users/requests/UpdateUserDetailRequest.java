package com.example.userservice.controller.users.requests;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateUserDetailRequest {
    @Email
    @NotBlank(message = "Please fill in an Email in")
    private String email;
    private String screenName;
    @NotBlank(message = "Please fill in a Username in")
    private String username;

}
